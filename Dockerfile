FROM node:9.10.0

WORKDIR /app


RUN npm config set registry https://verdaccio.2kloc.com
RUN npm install serve@10.0.2 -g

ADD package.json /app/package.json
ADD yarn.lock /app/yarn.lock
RUN yarn install

ADD . /app
RUN npm run build

CMD ["./start.sh"]
