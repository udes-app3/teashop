import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

import './Product.css';

class Product extends PureComponent {
    render() {
        const {title, img} = this.props;
        return (
            <div className="Product">
                <h1 className="title">{title}</h1>
                <img src={img} alt={title} />
            </div>
        );
    }
  }

  Product.propTypes = {
    title: PropTypes.string,
    img: PropTypes.string,
  };
  
  export default Product;