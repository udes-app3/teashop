import React, { Component } from 'react';
import CreditCardInput from 'react-credit-card-input';

import './App.css';
import tea from './tea.png';
import {Product} from './Product';

class App extends Component {
  state = {
    disabled: true,
    thanks: false,
    cardNumber: '',
    expiry: '',
    cvc: '',
  };

  thanksTimeout = null;

  render() {
    const {cardNumber, expiry, cvc, thanks, disabled} = this.state;

    const thanksMarkup = thanks && (
      <div>
        <h2>Merci pour votre achât</h2>
      </div>
    );

    const formMarkup = !thanks && (
      <form className="form" onSubmit={this.handleSubmit}>
        <CreditCardInput
          cardNumberInputProps={{ value: cardNumber, onChange: this.handleCardNumberChange }}
          cardExpiryInputProps={{ value: expiry, onChange: this.handleCardExpiryChange }}
          cardCVCInputProps={{ value: cvc, onChange: this.handleCardCVCChange }}
          fieldClassName="credit-card"
        />
        <button type="submit" disabled={disabled} className={(disabled ? 'disabled' : null)}>Payez maintenant</button>
      </form>
    );

    return (
      <div className="App">
        <Product title="Tea #1" img={tea} />
        {thanksMarkup}
        {formMarkup}
      </div>
    );
  }

  isDisabled(cardNumber = this.state.cardNumber, expiry = this.state.expiry, cvc = this.state.cvc) {
    return cardNumber === '' || expiry === '' || cvc === '';
  }

  handleCardNumberChange = (event) => {
    this.setState({
      cardNumber: event.target.value, 
      disabled: this.isDisabled(event.target.value),
    })
  };

  handleCardExpiryChange = (event) => {
    this.setState({
      expiry: event.target.value, 
      disabled: this.isDisabled(this.state.cardNumber, event.target.value),
    })
  };

  handleCardCVCChange = (event) => {
    this.setState({
      cvc: event.target.value,
      disabled: this.isDisabled(this.state.cardNumber, this.state.expiry, event.target.value),
    })
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const input = document.querySelector('.credit-card.is-invalid');
    if (!input) {
      this.setState({ thanks: true });
      this.thanksTimeout = setTimeout(this.resetThanks, 2000);
    }
  };

  resetThanks = () => {
    this.thanksTimeout = null;
    this.setState({ 
      disabled: true,
      thanks: false,
      cardNumber: '',
      expiry: '',
      cvc: '', 
    })
  }
}

export default App;
